# GitLab Multi-Project Pipeline & Helm crude demo

This repo integrates "upstream" microservices located within this same GitLab [group](https://gitlab.com/multi-project-pipeline-demo) using GitLabs's multi-project pipeline feature and Helm, while tracking version state in git. The benefit of this approach is that separate teams can maintain their own upstream repos individually in whatever manner they see fit, and a separate team can maintain their own e2e tests and control the promotion between environments in this repo ("gitops").

## Quick Start

### Prerequisite
You have your `kubectl` pointed to a cluster with the [ingress-nginx](https://kubernetes.github.io/ingress-nginx/) controller running.

OR

You can install [Kind](https://kind.sigs.k8s.io/) on your local machine and then run the script `./kind.sh` to launch a local cluster and also launch the ingress-nginx controller within it.

Also you need [Helm](https://helm.sh/) at the ready.

### Run it
Clone this repo and run `$ helm install my-multi-pipeline-ops-demo multi-pipeline-ops`

## How it works

All CI/CD operations are executed by GitLab's k8s-runner. Upstream projects are tested and then built as docker images using Kaniko and pushed to a container registry. On build success, the upstream project triggers this downstream repo's CI/CD, passing `$APPNAME` and `$APPTAG` variables.

These variables are used by Helm to update the current release. Once Helm rolls out the new k8s deployment to the staging namespace, the values exported as a text file and committed to this git repo. This keeps track of the current state of the aggregated release and allows users to run the entire app locally by cloning and running helm against the repo.

Next, a simple testing container is spun up to check that one of the microservices is accepting http connections. This is run against the actual staging environment, but could also be performed in a separate, disposable silo using KinD.

If the e2e test passes, then the develop branch (which corresponds to the staging namespace/environment) can be merged into master, which (if tagged) triggers the Helm command to deploy to the prod namespace/environment by using prod values.
